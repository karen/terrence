// Bootstrap budget bot
// No VC
// Buy if bankBalance > investment cost
// Expand if happiness is > 20
export default ({ state }) => {
  const { tweet, tick, turn, bankBalance, happiness } = state;
  if (tweet.type === "infra") {
    const investment = tweet.service.price;
    return investment > bankBalance;
  }
  if (tweet.type === "plot") {
    if (turn < 2) return false;
    return happiness < 11;
  }
  if (tweet.type === "intro") {
    if (tick === 0) return true;
    if (tick === 1) return false;
  }
  return true;
};
