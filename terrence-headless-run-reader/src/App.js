import { useState } from "react";
import Summary from "./components/summary";
import "./App.css";

function readFile(file) {
  const content = file.text();
  return content.then(JSON.parse);
}

function getFile(dataTransfer) {
  if (dataTransfer.files && dataTransfer.files.length === 1) {
    return dataTransfer.files[0];
  } else if (
    dataTransfer.items &&
    dataTransfer.items.length === 1 &&
    dataTransfer.items[0].kind === "file"
  ) {
    return dataTransfer.items[0].getAsFile();
  }
  return undefined;
}

function App() {
  const [loaded, setLoaded] = useState(true);
  const [summary, setSummary] = useState(undefined);

  const dropHandler = (ev) => {
    console.log("File(s) dropped");
    ev.preventDefault();
    const file = getFile(ev.dataTransfer);
    if (file) {
      setLoaded(false);
      console.log(file);
      readFile(file).then((s) => {
        setSummary(s);
        setLoaded(true);
      });
    }
  };

  const dragOverHandler = (ev) => {
    console.log("File(s) in drop zone");
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
  };

  return (
    <div className="App">
      <header className="App-header">
        <div>Citerate run reader {loaded ? "" : "(loading)"}</div>
      </header>
      <div
        className="App-dropzone"
        onDrop={dropHandler}
        onDragOver={dragOverHandler}
      >
        {summary ? (
          <>
            <Summary data={summary} />
          </>
        ) : (
          "Start by dropping a summary.json file here"
        )}
      </div>
    </div>
  );
}

export default App;
