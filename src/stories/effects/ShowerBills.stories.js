import "assets/main.css";

import BillsShower from "components/effects/billshower";

export default {
  title: "components/effects/billshower",
  component: BillsShower,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: true,
    },
  },
};

export const Primary = (props) => {
  return <BillsShower {...props} />;
};
