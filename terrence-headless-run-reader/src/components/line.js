import { ResponsiveLineCanvas as ResponsiveLine } from "@nivo/line";

const Line = ({ data, name }) => (
  <div style={{ height: "400px" }}>
    <ResponsiveLine
      data={data}
      colors={["#00502020"]}
      margin={{ top: 50, right: 20, bottom: 50, left: 80 }}
      xScale={{ type: "point" }}
      yScale={{
        type: "linear",
        min: "auto",
        max: "auto",
      }}
      yFormat={`>-s`}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickValues: [0, 50, 100, 150, 200],
        orient: "bottom",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: "Tick",
        legendOffset: 36,
        legendPosition: "middle",
      }}
      axisLeft={{
        orient: "left",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: name,
        legendOffset: -50,
        legendPosition: "middle",
        format: ">-0.3s",
      }}
      enablePoints={false}
      enableGridX={false}
      pointSize={10}
      pointColor={{ theme: "background" }}
      pointBorderWidth={2}
      pointBorderColor={{ from: "serieColor" }}
      pointLabelYOffset={-12}
      useMesh={true}
    />
  </div>
);

export default Line;
