import { Popup } from "components/generic/popup";
import { useInstallPrompt } from "hooks/pwaInstall";

export const InstallPopup = () => {
  const { isInstallable, promptInstall } = useInstallPrompt();

  return isInstallable ? (
    <Popup accept={promptInstall} show={isInstallable}>
      Add the game to your home screen?
    </Popup>
  ) : (
    <div />
  );
};
