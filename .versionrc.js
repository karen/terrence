const xml2js = require("xml2js");

const parseSync = (contents) => {
  let dataOut = undefined;
  xml2js.parseString(contents, (err, data) => {
    dataOut = data;
  });
  while (!dataOut) {}
  return dataOut;
};

const serializeSync = (obj) => {
  var builder = new xml2js.Builder();
  return builder.buildObject(obj);
};

const tracker = {
  filename: "config.xml",
  updater: {
    readVersion: function (contents) {
      const data = parseSync(contents);
      return data.widget.$.version;
    },
    writeVersion: function (contents, version) {
      const data = parseSync(contents);
      data.widget.$.version = version;

      return serializeSync(data);
    },
  },
};

module.exports = {
  bumpFiles: [tracker, "package.json"],
  packageFiles: [tracker],
};
