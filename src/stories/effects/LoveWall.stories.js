import "assets/main.css";

import LoveWall from "components/effects/lovewall";

export default {
  title: "components/effects/lovewall",
  component: LoveWall,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: true,
    },
  },
};

export const Primary = (props) => {
  return <LoveWall {...props} />;
};
