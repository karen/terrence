# Ways to contribute

Thank you for considering helping our project ❤️

There are multiple ways you can contribute to this game:

- with bug reports
- with code
- with game design or elements

## Contributing bug reports 🐞

At the heart of every project are bugs, issues and other inconsistencies. If you find anything that doesn't feel right, please [fill in a new issue](https://gitlab.com/karen/terrence/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Contributing code 👩‍💻

The game is made using web technologies. More specifically it is built with React. Commits should follow the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

Fork and open new MR's ⭐️

## Contributing game elements

You can update the configuration of the game:

- for updating infrastructure types metadata [infra-categories.json](https://gitlab.com/karen/terrence/-/blob/master/src/config/data/infra-categories.json)
- for updating infrastructure buildings [infra-card-definitions.json](https://gitlab.com/karen/terrence/-/blob/master/src/config/data/infra-card-definitions.json)
- for the winning conditions [goals.js](https://gitlab.com/karen/terrence/-/blob/master/src/config/goals.js)
- for the initial game values [state.js](https://gitlab.com/karen/terrence/-/blob/master/src/config/state.js)

If you want to update the game more deeply you can check [the state manager](https://gitlab.com/karen/terrence/-/blob/master/src/hooks/gamestate/index.js).

Of course, you can offer the changes directly through MR's 🚀

# Running the project locally

## Scripts

The project uses npm scripts for the various task related to the project. Here are the most used:

- `npm run start` starts the dev server, the builds, the tests and the storybook (recommended for coding)
- `npm run bump-version` creates the changelog from the commits, tag, push the changes. Then releases to the play store and the apple store.
- `npm run update-config` reads the data from the [configuration spreadsheet](https://docs.google.com/spreadsheets/d/1iDQhapdgYCDB8_O6RctFtjUwd1pdkjg3-kk-zt1PuNo/edit#gid=0) and updates the files. You need to commit the files.
- `npm run build:assets` takes the files from the assets folder, transform them and updates the app. You need to commit the changes.

## Headless mode

The game can be played by simple AIs to test the behaviour of the game. This is a three step process:

- do the run: `npm run headless:run -- --bot random` (there are other bots too, check src/cli/bot/index.js)
- generate the report: `npm run headless:report -- --folder runs/<date>/<bot>-<iteration>/` this generates a file called `summary.json`
- view the report: `npm run headless:viewer` this launches an app in your browser in which you can drag and drop the `summary.json` file

## Stores and apps

You need to have the certificates and keys properly configured. Check the fastlane
documentation and setup the environments variables.

# Folders

## Code

The code for the game is in the `src` folder. There are four main areas:

- `content` is for the landing page of the website
- `cli` is for the headless mode (see below)
- `stories` is for the storybook tests
- the other folders are used for the game

`index.js` is the main entry point of the whole website. This is where the setup is done.

`App.js` is the root of the routing. There are two routings, for the web version (with the editorial content) and the App/PWA version (without the editorial content).

## Game code

- `screens` contains the "pages" of the game: title screen, high score, about and the game itself
- the screens are made of components available in `components`
- some components relies on custom hooks which are in the `hooks` folder. Most notably the state management is built with hooks.
- pieces of code that are reusable but do not require to be in hooks are in `services`, like the code generating the tweets and stateless helper functions.
- configurable data and associated APIs are in the `config` folder. The content of `config/data` is updated with `npm run update-config` for the most part (except legacy and static thing like world cities)

# Code style

The whole game is built using functional style React components. It uses hooks extensively.

The state management is done using `useReducer`. The state of the game is split into two parts:

- the headless state that contains the simulation and the game rules
- the UI state which builds on the headless state to orchestrate the UI and the animations
