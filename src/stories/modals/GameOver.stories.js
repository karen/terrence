import "assets/main.css";

import gamestate from "stories/data/sample-data.json";

import GameOverModal from "components/modals/gameover";
import { storiesOf } from "@storybook/react";
import { InstallContext } from "hooks/pwaInstall";
import { action } from "@storybook/addon-actions";

// export default {
//   title: "components/modals/gameover",
//   component: GameOverModal,
//   argTypes: {
//     show: {
//       control: "boolean",
//       defaultValue: true,
//     },
//   },
// };

const story = storiesOf("components/modals/gameover");

const looseData = (looseCondition) => ({
  ...gamestate,
  isGameOver: true,
  isWin: false,
  gameOverStatus: {
    money: looseCondition === "money",
    time: looseCondition === "time",
    happiness: looseCondition === "happiness",
    noGrowth: looseCondition === "noGrowth",
    quit: looseCondition === "quit",
  },
});

["money", "time", "happiness", "noGrowth", "quit"].forEach((condition) => {
  story.add(`${condition} - with install`, () => (
    <InstallContext.Provider
      value={{
        promptEvent: {
          prompt: action("Display install prompt"),
        },
        updateInstallContext: () => {},
      }}
    >
      <GameOverModal gamestate={looseData(condition)} show={true} />
    </InstallContext.Provider>
  ));
  story.add(`${condition} - without install`, () => (
    <InstallContext.Provider value={null}>
      <GameOverModal gamestate={looseData(condition)} show={true} />
    </InstallContext.Provider>
  ));
});
