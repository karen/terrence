import "assets/main.css";

import LoveWall from "components/effects/lovewall2";
import { action } from "@storybook/addon-actions";

export default {
  title: "components/effects/lovewall2",
  component: LoveWall,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: false,
    },
    onRest: {
      table: {
        disable: true,
      },
    },
  },
};

export const Primary = (props) => {
  return <LoveWall {...props} onRest={action("on rest")} />;
};
