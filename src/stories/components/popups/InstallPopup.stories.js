import "assets/main.css";
import { InstallPopup } from "components/popups/install";
import { action } from "@storybook/addon-actions";
import { InstallContext } from "hooks/pwaInstall";

export default {
  title: "components/popups/install",
  component: InstallPopup,
  argTypes: {
    isInstallable: { control: "boolean", defaultValue: false },
    reload: { table: { disable: true } },
  },
};

export const Primary = (props) => {
  const { isInstallable } = props;
  // const prompt = action("Install prompt triggered");
  return (
    <InstallContext.Provider
      value={
        isInstallable
          ? {
              promptEvent: {
                prompt: action("Display install prompt"),
              },
              updateInstallContext: () => {},
            }
          : null
      }
    >
      <InstallPopup />
    </InstallContext.Provider>
  );
};
