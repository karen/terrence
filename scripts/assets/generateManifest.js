const fs = require("fs");
const path = require("path");
const cwd = process.cwd();

const workbox = require("workbox-build");

(async function () {
  const { manifestEntries, count, size, warnings } = await workbox.getManifest({
    globDirectory: "public",
    globPatterns: ["**/*.png"],
  });

  const content = JSON.stringify(manifestEntries, null, 2);

  try {
    fs.writeFileSync(path.join(cwd, "src/publicPNGfiles.json"), content, {
      flag: "w+",
    });
  } catch (e) {
    console.log(e);
  }

  console.log("Files added: ", count);
  console.log("size", size);
  console.log("warnings: ", warnings.join("\n"));
})();
