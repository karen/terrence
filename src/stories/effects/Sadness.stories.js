import "assets/main.css";

import Sadness from "components/effects/sadness";
import { action } from "@storybook/addon-actions";

export default {
  title: "components/effects/sadness",
  component: Sadness,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: false,
    },
    onRest: {
      table: {
        disable: true,
      },
    },
  },
};

export const Primary = (props) => {
  return <Sadness {...props} onRest={action("on rest")} />;
};
