import React from "react";

import Line from "./line";

const formatDataPoints = (serie) => serie.map((v, i) => ({ x: i, y: v }));
const seriesToNivoData = (series) =>
  series.map((s, id) => ({
    id,
    data: formatDataPoints(s),
  }));

const Statistics = ({ data }) => {
  return (
    <div style={{ textAlign: "left", padding: "20px" }}>
      <div>
        <div>Number of runs: {data.statistics.numberOfRuns}</div>
        <div>
          Number of actions played per game (mean):{" "}
          {data.statistics.meanTurnsPerGame}
        </div>
      </div>
      <div>
        <div>Win rate: {data.statistics.percentOfWins}%</div>
        {data.statistics.gameOverStatus && (
          <div>
            Game over conditions (a game may have multiple conditions)
            <ul>
              <li>No more cash: {data.statistics.gameOverStatus.money}</li>
              <li>Revolt: {data.statistics.gameOverStatus.happiness}</li>
              <li>No growth: {data.statistics.gameOverStatus.noGrowth}</li>
              <li>Time over: {data.statistics.gameOverStatus.time}</li>
              <li>Quit: {data.statistics.gameOverStatus.quit}</li>
            </ul>
          </div>
        )}
      </div>
      <div>
        <div>End game statistics</div>
        <div>Population (mean): {data.statistics.meanFinalPopulation}</div>
        <div>Cash (mean): {data.statistics.meanFinalCash}</div>
        <div>Happiness (mean): {data.statistics.meanFinalHappiness}</div>
      </div>
    </div>
  );
};

const Summary = ({ data }) => {
  const serieKeys = Object.keys(data.series);
  return (
    <div style={{ width: "100%" }}>
      <Statistics data={data} />
      {serieKeys.map((k) => (
        <Line key={k} data={seriesToNivoData(data.series[k])} name={k} />
      ))}
    </div>
  );
};

export default Summary;
