import emoji from "react-easy-emoji";

export default function svgEmoji(input, opts) {
  return emoji(input, {
    baseUrl: "https://twemoji.maxcdn.com/2/svg/",
    ext: ".svg",
    size: "",
    ...opts,
  });
}
