import WinModal from "components/modals/win";

import "assets/main.css";

import winGamestate from "../data/sample-win.json";
import { InstallContext } from "hooks/pwaInstall";
import { action } from "@storybook/addon-actions";

export default {
  title: "components/modals/win",
  component: WinModal,
  argTypes: {
    show: { control: "boolean", defaultValue: true },
  },
};

export const WinModalWithInstallPrompt = (props) => (
  <InstallContext.Provider
    value={{
      promptEvent: {
        prompt: action("Display install prompt"),
      },
      updateInstallContext: () => {},
    }}
  >
    <WinModal
      gamestate={winGamestate}
      history={[winGamestate, winGamestate, winGamestate]}
      {...props}
    />
  </InstallContext.Provider>
);

export const WinModalWithoutInstallPrompt = (props) => (
  <InstallContext.Provider value={null}>
    <WinModal
      gamestate={winGamestate}
      history={[winGamestate, winGamestate, winGamestate]}
      {...props}
    />
  </InstallContext.Provider>
);
