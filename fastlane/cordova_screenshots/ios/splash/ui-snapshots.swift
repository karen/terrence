//
//  ui_snapshots.swift
//  ui-snapshots
//
//  Created by Adrian Regan on 07/04/2017.
//
//

import XCTest

class ui_snapshots: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        let app = XCUIApplication()
               
        setupSnapshot(app)

        app.launch()
 
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSnapshots() {
        snapshot("app-launch")
        
        let webViewsQuery = XCUIApplication().webViews.webViews.webViews
        sleep(2)
        
        snapshot("title-screen")
        
        sleep(2)

        webViewsQuery.staticTexts["New game"].tap()
        sleep(2)
        
        webViewsQuery.buttons["Let's build!"].tap()
        sleep(4)
        
        // click on the first city
        webViewsQuery.buttons.element(boundBy: 0).tap()
        
        sleep(2)
        
        let buildHereStaticText = webViewsQuery.staticTexts["BUILD HERE?"]
        buildHereStaticText.swipeLeft()
        
        sleep(2)
   
        let buildHereStaticText2 = webViewsQuery.staticTexts["BUILD HERE?"]
        buildHereStaticText2.swipeRight()

        sleep(2)

        snapshot("game-screen")
        
        webViewsQuery.staticTexts["50%"].tap()
        sleep(2)

        let element = webViewsQuery.otherElements["Citerate"].children(matching: .other).element
        element.children(matching: .button).element(boundBy: 1).tap()
        
        snapshot("menu-1")
        element.children(matching: .button).element(boundBy: 2).tap()
        snapshot("menu-2")
        element.children(matching: .button).element(boundBy: 3).tap()
        snapshot("menu-3")

        
    }
    
}
