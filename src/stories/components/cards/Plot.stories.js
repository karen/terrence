import { useState } from "react";

import "assets/main.css";
import PlotCard from "components/cards/plot";
import { getPlotSaleTweet } from "services/tweetGenerator/plot";

import gamestate from "stories/data/sample-data.json";

export default {
  title: "components/cards/plot",
  component: PlotCard,
  argTypes: {
    happiness: {
      control: "range",
      defaultValue: 50,
      min: 0,
      max: 100,
    },
  },
};

export const Pop0 = ({ happiness }) => {
  const state = {
    tweet: getPlotSaleTweet({
      ...gamestate,
      happiness,
      population: 0,
      availableServices: [],
    }),
  };
  const [key, setKey] = useState(0);
  const onSwipe = () => {
    setKey(key + 1);
  };
  return (
    <PlotCard
      gamestate={state}
      key={key}
      height={300}
      left={onSwipe}
      right={onSwipe}
    />
  );
};

export const Pop50 = ({ happiness }) => {
  const state = {
    tweet: getPlotSaleTweet({
      ...gamestate,
      happiness,
      population: 50,
      availableServices: [
        "SharedHotspot",
        "IndividualGenerator",
        "FirstAidKit",
        "Wells",
      ],
    }),
  };
  const [key, setKey] = useState(0);
  const onSwipe = () => {
    setKey(key + 1);
  };
  return (
    <PlotCard
      gamestate={state}
      key={key}
      height={300}
      left={onSwipe}
      right={onSwipe}
    />
  );
};

export const Pop1000 = ({ happiness }) => {
  const state = {
    tweet: getPlotSaleTweet({
      ...gamestate,
      happiness,
      population: 1000,
      availableServices: [
        "SharedHotspot",
        "IndividualGenerator",
        "FirstAidKit",
        "Wells",
        "UnpavedRoads",
        "BookHouse",
        "FlowerBed",
        "Starlink",
        "FireExtinguishers",
        "NeighborhoodWatch",
      ],
    }),
  };
  const [key, setKey] = useState(0);
  const onSwipe = () => {
    setKey(key + 1);
  };
  return (
    <PlotCard
      gamestate={state}
      key={key}
      height={300}
      left={onSwipe}
      right={onSwipe}
    />
  );
};
