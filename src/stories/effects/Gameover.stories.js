import "assets/main.css";

import { GameoverEffect } from "components/effects/gameover";

export default {
  title: "components/effects/gameover",
  component: GameoverEffect,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: false,
    },
  },
};

export const Primary = (props) => {
  return <GameoverEffect {...props} />;
};
