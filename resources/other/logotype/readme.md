The logo is transformed manually using SVGR UI into a react component that
can be found in 'src/components/logo.js'

The splashscreen is transformed into a PNG and then saved as resources/splashscreen.png.
This file is then used by cordova-res, to generate the platform specific splashscreen
configuration and images.
