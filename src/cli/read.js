// import React from "react";

import fs from "fs";
import path from "path";

import { Command } from "commander";
const program = new Command();

program.requiredOption(
  "--folder <folder>",
  "Name of the bot to use for the run"
);

program.parse(process.argv);

const { folder } = program;
const files = fs.readdirSync(folder);
analyze(files);

async function analyze(files) {
  let sum = {
    turn: 0,
    win: 0,
    gameover: 0,
    unfinished: 0,
    finalPopulation: 0,
    cash: 0,
    happiness: 0,
    gameOverStatus: {
      money: 0,
      time: 0,
      happiness: 0,
      noGrowth: 0,
      quit: 0,
    },
  };

  let series = {
    cash: [],
    happiness: [],
    plotPrice: [],
    population: [],
  };

  console.log("Reading files");

  const dataRuns = files
    .filter((f) => f !== "summary.json")
    .map((f) => fs.readFileSync(path.join(folder, f)))
    .map((c) => JSON.parse(c));

  console.log("Computing summary");

  function extractSerie(d, variableName) {
    const serie = d.history.map((t) => t[variableName]);
    serie.push(d.gamestate[variableName]);
    return serie;
  }

  dataRuns.forEach((d) => {
    sum.turn += d.history.length;
    sum.win += d.end === "win" ? 1 : 0;
    sum.gameover += d.end === "gameover" ? 1 : 0;
    sum.unfinished += d.end === "unfinished" ? 1 : 0;
    sum.finalPopulation += d.gamestate["population"];
    sum.cash += d.gamestate["bankBalance"];
    sum.happiness += d.gamestate["happiness"];
    sum.gameOverStatus.money += d.gamestate["gameOverStatus"].money ? 1 : 0;
    sum.gameOverStatus.time += d.gamestate["gameOverStatus"].time ? 1 : 0;
    sum.gameOverStatus.happiness += d.gamestate["gameOverStatus"].happiness
      ? 1
      : 0;
    sum.gameOverStatus.noGrowth += d.gamestate["gameOverStatus"].noGrowth
      ? 1
      : 0;
    sum.gameOverStatus.quit += d.gamestate["gameOverStatus"].quit ? 1 : 0;

    series.cash.push(extractSerie(d, "bankBalance"));
    series.happiness.push(extractSerie(d, "happiness"));
    series.plotPrice.push(extractSerie(d, "plotPrice"));
    series.population.push(extractSerie(d, "population"));
  });

  const statistics = {
    numberOfRuns: dataRuns.length,
    meanTurnsPerGame: sum.turn / dataRuns.length,
    percentOfWins: (sum.win / dataRuns.length) * 100,
    percentOfGameOver: (sum.gameover / dataRuns.length) * 100,
    percentOfUnfinished: (sum.unfinished / dataRuns.length) * 100,
    meanFinalPopulation: sum.finalPopulation / dataRuns.length,
    meanFinalCash: sum.cash / dataRuns.length,
    meanFinalHappiness: sum.happiness / dataRuns.length,
    gameOverStatus: sum.gameOverStatus,
  };

  console.log("Writing summary");

  const outputFile = path.join(folder, "summary.json");
  fs.writeFileSync(outputFile, JSON.stringify({ statistics, series }, null, 2));

  console.log(`Summary exported to ${outputFile}`);
}
