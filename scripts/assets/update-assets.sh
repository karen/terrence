#!/bin/sh

if ! command -v jq &> /dev/null
then
    echo "jq could not be found and it is required for this script"
    exit
fi

echo "SVG to PNG"

svgexport scripts/assets/svgexport.datafile.js

echo "Prepare cordova resources (icon and splashscreen)"

cordova-res

echo "Format config files"

jq '.|=sort_by(.type, .building)' src/config/data/infra-card-illustrations.json > out.json
mv out.json src/config/data/infra-card-illustrations.json

jq '.|=sort_by(.id)' src/config/data/infra-categories.json > out.json
mv out.json src/config/data/infra-categories.json
