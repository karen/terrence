import { getCurrentThreshold } from "config/contracts";

const { makeTweet, makeAnswerTweet } = require("./factory");

const publicURL = process.env.PUBLIC_URL;

const isNextTweetAContractUpdate = (gamestate) => {
  // The tweet in the game state is the last one because we are making the next
  const lastTweet = gamestate.tweet;
  return (
    (lastTweet.type === "plot" || lastTweet.type === "infra") &&
    getCurrentThreshold(gamestate) !== undefined
  );
};

const tweets = [
  "No one wants to come yet. Maybe we should give some ownership to the first citizens?",
  "Maybe we can stop giving part of the city to new citizens?",
  "What about making money from the new plot sales?",
];

const getContractUpdateTweet = (gamestate) => {
  const currentThreshold = getCurrentThreshold(gamestate);
  const message = tweets[currentThreshold];

  return makeTweet({
    type: "contract",
    author: "@landOwner2",
    message,
    impact: {
      contractLevel: currentThreshold + 1,
    },
    actionHints: {
      left: "Great idea!",
      right: "Sounds awesome!",
    },
    illustrationPath: `${publicURL}/other-cards/StockGiveaway.png`,
  });
};

const answers = {
  left: "Let's update the terms!",
  right: "Let's update the terms!",
};
const getContractUpdateAnswer = (action, originalTweet) => {
  if (originalTweet.type !== "contract") return undefined;

  return [makeAnswerTweet(answers[action], originalTweet)];
};

export {
  isNextTweetAContractUpdate,
  getContractUpdateAnswer,
  getContractUpdateTweet,
};
